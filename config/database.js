module.exports = ({ env }) => ({
  defaultConnection: "default",
  connections: {
    default: {
      connector: "bookshelf",
      settings: {
        client: "mysql",
        host: env('DB_HOST'),
        port: env('DB_PORT'),
        username: env('DB_USER'),
        password: env('DB_PASSWORD'),
        database: env('DB_DATABASE'),
      },
      options: {
        useNullAsDefault: true,
      },
    },
  },
});